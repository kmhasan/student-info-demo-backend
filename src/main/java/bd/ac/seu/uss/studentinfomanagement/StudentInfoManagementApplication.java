package bd.ac.seu.uss.studentinfomanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudentInfoManagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(StudentInfoManagementApplication.class, args);
	}
}
