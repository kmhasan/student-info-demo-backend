package bd.ac.seu.uss.studentinfomanagement.repository;

import bd.ac.seu.uss.studentinfomanagement.model.Student;
import org.bson.types.ObjectId;
import org.hibernate.validator.constraints.Email;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends MongoRepository<Student, ObjectId> {
    public Student findByStudentId(long studentId);
    public Student findByEmailAddress(@Email String emailAddress);
}
