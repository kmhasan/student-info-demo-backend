package bd.ac.seu.uss.studentinfomanagement.service;

import bd.ac.seu.uss.studentinfomanagement.model.Student;
import bd.ac.seu.uss.studentinfomanagement.repository.StudentRepository;
import org.hibernate.validator.constraints.Email;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@Service
public class StudentService {
    @Autowired
    private StudentRepository studentRepository;

    /**
     * CREATE operation -- only creates/saves a student record if it is a new student object
     * @param student a Student object
     * @return the student object saved or returns null if the save operation fails
     */
    public Student createStudent(Student student) {
        Student foundStudent = studentRepository.findByStudentId(student.getStudentId());
        if (foundStudent == null)
            return studentRepository.save(student);
        else return null;
    }

    /**
     * READ operation to read the records of all students
     * @return a List of all the students
     */
    public List<Student> readAllStudents() {
        return studentRepository.findAll();
    }

    /**
     * READ operation to read the record of a single student based on the student's ID
     * @param studentId the ID of the student to search
     * @return a Student object if such a student was found, or null instead
     */
    public Student readOneStudent(long studentId) {
        return studentRepository.findByStudentId(studentId);
    }

    /**
     * READ operation to read the record of a single student based on the student's Email address
     * @param emailAddress the emailAddress of the student to search
     * @return a Student object if such a student was found, or null instead
     */
    public Student readOneStudent(@Email String emailAddress) {
        return studentRepository.findByEmailAddress(emailAddress);
    }

    /**
     * UPDATE operation to update the record of a single student if such a student exists
     * @param student the student object whose record needs to be updated
     * @throws NoSuchElementException if such a student does not exist
     */
    public Student updateStudent(Student student) {
        Student foundStudent = studentRepository.findOne(student.getObjectId());
        if (foundStudent != null)
            return studentRepository.save(student);
        else throw new NoSuchElementException();
    }

    /**
     * DELETE operation to delete the record of a single student if such a student exists
     * @param studentId the ID of the student to delete
     * @throws NoSuchElementException if such a student does not exist
     */
    public boolean deleteStudent(long studentId) {
        Student foundStudent = studentRepository.findByStudentId(studentId);
        if (foundStudent != null) {
            studentRepository.delete(foundStudent);
            return studentRepository.findByStudentId(studentId) == null;
        }
        else throw new NoSuchElementException();
    }
}
