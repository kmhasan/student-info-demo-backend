package bd.ac.seu.uss.studentinfomanagement.controller;

import bd.ac.seu.uss.studentinfomanagement.model.Address;
import bd.ac.seu.uss.studentinfomanagement.model.Phone;
import bd.ac.seu.uss.studentinfomanagement.model.Student;
import bd.ac.seu.uss.studentinfomanagement.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/v${api.version}")
public class StudentController {
    @Autowired
    private StudentService studentService;

    @GetMapping(value = "students")
    public List<Student> getAllStudents() {
        return studentService.readAllStudents();
    }

    @PostMapping(value = "student/create")
    public Student createStudent(@RequestParam long studentId,
                                 @RequestParam String studentName,
                                 @RequestParam String emailAddress) {
        Student student = new Student();
        student.setStudentId(studentId);
        student.setStudentName(studentName);
        student.setEmailAddress(emailAddress);
        return studentService.createStudent(student);
    }

    @PostMapping(value = "student/create_from_json")
    public Student createStudent(@RequestBody Student student) {
        return studentService.createStudent(student);
    }

    @PutMapping(value = "student/update_from_json")
    public Student updateStudent(@RequestBody Student student) {
        return studentService.updateStudent(student);
    }

    @PostMapping(value = "student/{studentId}/addphone")
    public Phone addPhoneNumber(@PathVariable long studentId,
                               @RequestParam int countryCode,
                               @RequestParam int areaCode,
                               @RequestParam int number) {
        Student student = studentService.readOneStudent(studentId);
        if (student != null) {
            Phone phone = new Phone(countryCode, areaCode, number);
            student.addPhone(phone);
            studentService.updateStudent(student);
            return phone;
        } else return null;
    }

    @PostMapping(value = "student/{studentId}/addaddress")
    public Address addAddress(@PathVariable long studentId,
                           @RequestParam String streetAddress,
                           @RequestParam String city,
                           @RequestParam String country,
                           @RequestParam String postalCode) {
        Student student = studentService.readOneStudent(studentId);
        if (student != null) {
            Address address = new Address(streetAddress, city, country, postalCode);
            student.addAddress(address);
            studentService.updateStudent(student);
            return address;
        } else return null;
    }

    @DeleteMapping(value = "student/{studentId}/delete")
    public boolean deleteStudent(@PathVariable long studentId) {
        return studentService.deleteStudent(studentId);
    }
}
